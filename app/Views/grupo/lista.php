<?= $this->extend('templates/default') ?>

//Disponemos de 3 secciones
<?= $this->section('head_title') ?>
    <?= $title?>
<?= $this->endSection() ?>

<?= $this->section('title') ?>
    <?= $title?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
    <table class="table table-striped" id="myTable">
        <thead>
            <tr>
                <th>
                    Código
                </th>
                <th>
                    Grupo
                </th>
                <th>
                    Acciones
                </th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($grupos as $grupo): ?>
                <tr>
                    <td>
                        <?= $grupo->codigo ?>
                    </td>
                    <td>
                        <?= $grupo->nombre ?>
                    </td>
                    <td class="text-right">
                        <a href="<?=site_url('grupo/editar/'.$grupo->id)?>">
                            <span class="bi bi-pencil-square" title="Editar el grupo"></span>
                        </a>
                         <a href="<?=site_url('grupo/borrar/'.$grupo->id)?>" onclick="return confirm('¿Deseas borrar el grupo <?=$grupo->nombre?>?')">
                            <span class="bi bi-eraser-fill" title="Editar el grupo"></span>
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?= $this->endSection() ?>

