<?php

/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */
?>
<html>
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">

        <title><?= $title?></title>
    </head>
    <body>
        <h1 class="text-primary"><?= $title?></h1>
        <form action="<?= site_url('nombre_alumno/nombre_alumno')?>" method="post">
            <label for="texto" class="text-success">Introduce el texto a buscar: </label>
            <input type="text" name="texto" value="Pon un valor" id="texto" />
            <input type="submit" name="enviar" value="Enviar" />
        </form>
    </body>
</html>
