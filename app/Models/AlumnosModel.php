<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

/**
 * Description of Alumnos
 *
 * @author jbarrachinab
 */
namespace App\Models;
use CodeIgniter\Model;


class AlumnosModel extends Model {
    /* definimos los parámetros de la tabla a la que
     * queremos acceder
     */
    protected $table = 'alumnos';
    protected $primaryKey = 'id';
    protected $returnType = 'object';
    protected $allowedFields = ['NIA','nombre','apellido1','apellido2'];
    
}
