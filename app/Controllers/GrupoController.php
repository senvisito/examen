<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Controllers;
use App\Models\GrupoModel; //decimos donde está

/**
 * Description of AlumnoController
 *
 * @author jose
 */
class GrupoController extends BaseController {
    
        public function index(){
        $grupoModel = new GrupoModel();
        /*echo '<pre>';
        print_r($grupoModel->findAll());
        echo '</pre>';*/
        $data['title'] = 'Listado de Grupos';
        $data['grupos'] = $grupoModel->findAll();
        return view('grupo/lista',$data);
    }
    
    public function formEdit($id){
        helper('form');
        $grupoModel = new GrupoModel();
        $data['title'] = 'Modificar Grupo';
        $data['grupo'] = $grupoModel->find($id);
        /*echo '<pre>';
        print_r($data);
        echo '</pre>';*/
        return view('grupo/formEdit',$data);
    }
    
    public function actualiza($id){
        //recoger datos
        $grupo = $this->request->getPost();
        unset($grupo['botoncito']);
        /*echo '<pre>';
        echo $id;
        print_r($grupo);
        echo '</pre>';*/
        $grupoModel = new GrupoModel();
        $grupoModel->update($id,$grupo);
        return redirect()->to('grupo/lista');
    }


}

